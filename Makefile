up:
	docker-compose up --build -d
stop:
	docker-compose stop
log:
	docker-compose logs -f --tail 100
down:
	docker-compose down
restart: stop up
