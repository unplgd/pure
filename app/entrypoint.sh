#!/bin/bash

echo "waiting for postgres"
while ! nc -z $POSTGRES_HOST $POSTGRES_PORT; do
  sleep 0.1
done
echo "postgres started"

python manage.py migrate
python manage.py collectstatic --noinput

exec "$@"
