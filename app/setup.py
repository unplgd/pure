#!/usr/bin/env python

from setuptools import setup

setup(
      name='app',
      version='0.1',

      install_requires=[
            'Django<4',
            'gunicorn',
            'psycopg2-binary',
            'djangorestframework',
      ],
      extras_require={
            'local': [
                  'python-dotenv',
            ]
      }
)
