from random import randint

from rest_framework.views import APIView
from rest_framework.response import Response

from main.models import Token
from main.utils import sent_email


def get_response(status, info=''):
    return Response({
        'status': status,
        'info': info,
    })


def send_verification_code(token):
    token_item = Token.objects.get(value=token)
    if not token_item.verification_code:
        token_item.verification_code = ''.join([str(randint(0, 9)) for _ in range(6)])  # todo add uniq
        token_item.save()
    sent_email(token_item.owner, 'verification code', token_item.verification_code)
    return True


class SendVerificationCode(APIView):
    """
    Parameters
    ----------
    email : string
    token : string
    """
    def post(self, request):
        email = request.data.get('email')
        token = request.data.get('token')

        if not email:
            return get_response(4, 'no email')

        if not token:
            return get_response(5, 'no token')

        if not Token.objects.filter(value=token, token_type='gb').exists():
            return get_response(1, 'invalid token')

        token_item = Token.objects.get(value=token, token_type='gb')

        if token_item.activated:
            return get_response(2, 'token already activated')

        token_item.owner = email
        token_item.save()

        if not send_verification_code(token):
            return get_response(3, 'cannot send code on this email')

        return get_response(0)


class SubmitToken(APIView):
    """
    Parameters
    ----------
    verification_code : string
    date : string
    """
    def post(self, request):
        verification_code = request.data.get('verification_code')
        date = request.data.get('date')

        if not verification_code:
            return get_response(4, 'no verification_code')

        if not date:
            return get_response(5, 'no date')

        if not Token.objects.filter(verification_code=verification_code, token_type='gb').exists():
            return get_response(1, 'invalid code')

        token_item = Token.objects.get(verification_code=verification_code, token_type='gb')

        if token_item.activated:
            return get_response(2, 'token already activated')

        token_item.date = date
        token_item.activated = True
        token_item.save()

        return get_response(0)


class SendVerificationCodeInvest(APIView):
    """
    Parameters
    ----------
    email : string
    token : string
    """
    def post(self, request):
        email = request.data.get('email')
        token = request.data.get('token')

        if not email:
            return get_response(4, 'no email')

        if not token:
            return get_response(5, 'no token')

        if not Token.objects.filter(value=token, token_type='in').exists():
            return get_response(1, 'invalid token')

        token_item = Token.objects.get(value=token, token_type='in')

        if token_item.activated:
            return get_response(2, 'token already activated')

        token_item.owner = email
        token_item.save()

        if not send_verification_code(token):
            return get_response(3, 'cannot send code on this email')

        return get_response(0)


class SubmitTokenInvest(APIView):
    """
    Parameters
    ----------
    verification_code : string
    """
    def post(self, request):
        verification_code = request.data.get('verification_code')

        if not verification_code:
            return get_response(4, 'no verification_code')

        if not Token.objects.filter(verification_code=verification_code, token_type='in').exists():
            return get_response(1, 'invalid code')

        token_item = Token.objects.get(verification_code=verification_code, token_type='in')

        if token_item.activated:
            return get_response(2, 'token already activated')

        token_item.activated = True
        token_item.save()

        return get_response(0)
