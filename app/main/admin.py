from django.contrib import admin

from main.models import Token, Setting

admin.site.register(Token)
admin.site.register(Setting)
