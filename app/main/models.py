from django.db import models

TOKEN_TYPE_CHOICES = [
    ('gb', 'get bush'),
    ('in', 'invest now'),
]


class Token(models.Model):
    value = models.CharField(max_length=128, unique=True)
    token_type = models.CharField(max_length=5, choices=TOKEN_TYPE_CHOICES, default='gb')
    owner = models.EmailField(blank=True)
    verification_code = models.CharField(max_length=128, blank=True)
    activated = models.BooleanField(default=False)
    date = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return self.value


class Setting(models.Model):
    key = models.CharField(max_length=100)
    value = models.JSONField()

    def __str__(self):
        return self.key
