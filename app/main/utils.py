import smtplib
import ssl
from email.message import EmailMessage

from main.models import Setting


def get_email_setting():
    email_setting = Setting.objects.get(key='email_setting')
    return email_setting.value


def sent_email(receiver_email, subject='', message=''):
    email_setting = get_email_setting()

    smtp_server = email_setting['smtp_server']
    port = email_setting['port']
    sender_email = email_setting['email']
    password = email_setting['password']

    msg = EmailMessage()
    msg.set_content(message)
    msg['Subject'] = subject
    msg['From'] = sender_email
    msg['To'] = receiver_email

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.send_message(msg)
