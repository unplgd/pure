from django.urls import path

from main import views

urlpatterns = [
    path('get/send_verification_code/', views.SendVerificationCode.as_view()),
    path('invest/send_verification_code/', views.SendVerificationCodeInvest.as_view()),
    path('get/submit_token/', views.SubmitToken.as_view()),
    path('invest/submit_token/', views.SubmitTokenInvest.as_view()),
]
